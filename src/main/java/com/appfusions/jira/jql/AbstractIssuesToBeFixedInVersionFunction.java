package com.appfusions.jira.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.NotNull;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import java.util.Collection;
import java.util.List;

public abstract class AbstractIssuesToBeFixedInVersionFunction extends AbstractJqlFunction {
    private static final Logger log = Logger.getLogger(AbstractIssuesToBeFixedInVersionFunction.class);

    private final VersionManager versionManager;
    private final PermissionManager permissionManager;

    public AbstractIssuesToBeFixedInVersionFunction(VersionManager versionManager, PermissionManager permissionManager) {
        this.versionManager = versionManager;
        this.permissionManager = permissionManager;
    }

    @Override
    public List<QueryLiteral> getValues(@NotNull QueryCreationContext queryCreationContext, @NotNull FunctionOperand operand, @NotNull TerminalClause terminalClause) {
        Collection<Project> projects = permissionManager.getProjectObjects(Permissions.BROWSE, queryCreationContext.getUser());

        DateTime date = getDateFrom(operand);

        Collection<Version> versions = findAllVersionsFrom(projects, date);

        Collection<Issue> issues = findAllIssuesAssignedToBeFixedIn(versions);

        final List<QueryLiteral> literals = Lists.newArrayList();

        for (Issue issue : issues) {
            literals.add(new QueryLiteral(operand, issue.getId()));
        }

        return literals;
    }


    private Collection<Issue> findAllIssuesAssignedToBeFixedIn(Collection<Version> versions) {
        List<Issue> issues = Lists.newArrayList();
        for (Version version : versions) {
            issues.addAll(versionManager.getIssuesWithFixVersion(version));
        }
        return issues;
    }

    private Collection<Version> findAllVersionsFrom(Collection<Project> projects, DateTime date) {
        List<Version> versions = Lists.newArrayList();
        for(Project project : projects) {
            Collection<Version> projectVersions = versionManager.getVersions(project);
            for (Version version : projectVersions) {
                if (!version.isArchived() && !version.isReleased()) {
                    if (new DateTime(version.getReleaseDate()).isBefore(date)) {
                        log.debug("Version " + version.getName() + " is set to be released before " + date.toString() + "   :   " + version.getReleaseDate().toString());
                        versions.add(version);
                    }
                }
            }
        }
        return versions;
    }

    protected abstract DateTime getDateFrom(@NotNull FunctionOperand operand);

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.ISSUE;
    }
}
