package com.appfusions.jira.jql;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.NotNull;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

public class IssuesToBeFixedInVersionsBeforeEndOfWeekFunction extends AbstractIssuesToBeFixedInVersionFunction {
    private static final Logger log = Logger.getLogger(IssuesToBeFixedInVersionsBeforeEndOfWeekFunction.class);

    public IssuesToBeFixedInVersionsBeforeEndOfWeekFunction(VersionManager versionManager, PermissionManager permissionManager) {
        super(versionManager, permissionManager);
    }

    @Override
    protected DateTime getDateFrom(@NotNull FunctionOperand operand) {
        return DateTime.now().withDayOfWeek(DateTimeConstants.SUNDAY).plusDays(1);
    }

    @Override
    public MessageSet validate(User user, @NotNull FunctionOperand operand, @NotNull TerminalClause terminalClause) {
        MessageSet messages = new MessageSetImpl();
        if (operand.getArgs().size() != 0) {
            messages.addErrorMessage(getI18n().getText("com.appfusions.jira.misc.jql.no.arguments.expected"));
            return messages;
        }

        return messages;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 0;
    }
}
