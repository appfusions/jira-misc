package com.appfusions.jira.jql;

import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.NotNull;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import java.util.List;

public class IssuesToBeFixedVersionsBeforeDateFunction extends AbstractIssuesToBeFixedInVersionFunction {
    private static final Logger log = Logger.getLogger(IssuesToBeFixedVersionsBeforeDateFunction.class);

    public IssuesToBeFixedVersionsBeforeDateFunction(VersionManager versionManager, PermissionManager permissionManager) {
        super(versionManager, permissionManager);
    }

    @Override
    public MessageSet validate(User user, @NotNull FunctionOperand operand, @NotNull TerminalClause terminalClause) {
        MessageSet messages = new MessageSetImpl();
        
        List<String> args = operand.getArgs();

        if (args.size() != 1) {
            messages.addErrorMessage(getI18n().getText("com.appfusions.jira.misc.jql.invalid.argument"));
            return messages;
        }
        
        try {
            getDateFrom(operand.getArgs().get(0));
        }
        catch (Exception e) {
            messages.addErrorMessage(getI18n().getText("com.appfusions.jira.misc.jql.invalid.date", e.getMessage()));
        }

        return messages;
    }

    private DateTime getDateFrom(String dateArgument) {
        log.info("Date given is " + dateArgument);

        if (StringUtils.isBlank(dateArgument)) {
            return DateTime.now();
        }

        //Try a duration
        try {
            long duration = DateUtils.getDurationWithNegative(dateArgument);
            return DateTime.now().plus(duration * 1000);
        }
        catch(InvalidDurationException e) {
            log.debug("Invalid duration. Will try date parsing");
        }
        
        try {
            return DateTime.parse(dateArgument);
        }
        catch (IllegalArgumentException e) {
            log.warn(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    protected DateTime getDateFrom(@NotNull FunctionOperand operand) {
        return getDateFrom(operand.getArgs().get(0));
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 1;
    }
}
